#-*- coding:utf-8 -*-
import requests
import json
# xml을 dict 형태로 변경하기 위한 라이브러리
import xmltodict

#get으로 요청할때, 인자로 들어간 한글이 깨지는 문제가 발생. : 해당 뮌제를 해결 하기 위하여 사용하는 라이브러리

dust_api_keys = 'zHKzBSmpIIz6sd3aUeDAW5Ee7BoMdjVN%2FF9yTZFDyHeYzL6sKz44TA3sypR0Sg%2BPbqtGX5lRNE0Yj3MvI6aJ0w%3D%3D'


#카카오 Developers key [ 1일 1만건 가능]
#관련 API 링크
# https://developers.kakao.com/docs/latest/ko/local/dev-guide
KaKaoRestAPIKey = 'ea2dfc54f13d1eeaeca07fd6fcf11c53'
Host = 'https://dapi.kakao.com'
APIAddr ='/v2/local/geo/coord2address.json?input_coord=WGS84'

def coord2addres(lon, lat):
    # lon = x
    # lat = y
    global KaKaoRestAPIKey, Host, APIAddr

    header = {"Authorization": str("KakaoAK " + KaKaoRestAPIKey)}
    url = Host + APIAddr+"&x="+str(lon)+"&y="+str(lat)
    res = requests.get(url, headers=header)
    if res.status_code == 200:
        addr = res.json()
        region1st = addr['documents'][0]['address']['region_1depth_name']
        region3rd = addr['documents'][0]['address']['region_3depth_name']

        if len(region1st) == 3:
            region1st = region1st[:-1]
        elif len(region1st) > 3:
            region1st = region1st[:2]

    print(region1st, region3rd)
    return region1st, region3rd

def DetectNearCites(sido, stationName):
    #현재 주소를 TM 체계의 좌표로 바꾸기 위한 API 주소
    GetTMPositon_API = 'http://openapi.airkorea.or.kr/openapi/services/rest/MsrstnInfoInqireSvc/getTMStdrCrdnt'
    # TM 체계의 주소로 가까운 측정소의 위치를 찾기 위한 API 주소
    GetNearDetector_API = 'http://openapi.airkorea.or.kr/openapi/services/rest/MsrstnInfoInqireSvc/getNearbyMsrstnList'

    global dust_api_keys

    # 1. 좌표를 얻은 "동"이름으로 TM 좌표를 얻음.
    GetTMURL = GetTMPositon_API+"?ServiceKey="+dust_api_keys+"&umdName="+stationName
    GetTMres = requests.get(GetTMURL)

    if GetTMres.status_code == 200:
        TMDataList = xml2Dict(GetTMres.text)
        if TMDataList['response']['body']['items'] == None:
            return False
        else:
            TMDataList = xml2Dict(GetTMres.text)['response']['body']['items']['item']
    else:
        return False

    tmX = tmY = None
    #해당 동이 포함된 리스트를 탐색
    for sidoindex in TMDataList:
        if sidoindex['sidoName'][:2] == sido:
            tmX = sidoindex['tmX']
            tmY = sidoindex['tmY']

    # 2. 이렇게 얻은 tmX,tmY 좌표를 이용하여, 가까운 측정소 탐색
    GetStationURL = GetNearDetector_API + "?ServiceKey="+dust_api_keys+"&tmX="+tmX+"&tmY="+tmY
    GetStationres = requests.get( GetStationURL )

    StationList = []
    if GetStationres.status_code == 200:
        NearStationsList = xml2Dict(GetStationres.text)['response']['body']['items']['item']

        for StationIndex in NearStationsList:
            StationList.append(StationIndex['stationName'])
        return StationList
    return False

def Data2Dict(xmlDict):
    resultDict = {}
    infoList = xmlDict['response']['body']['items']['item']

    for stationIndex in infoList:
        resultDict.setdefault(stationIndex['stationName'], {})

        resultDict[stationIndex['stationName']].setdefault('o3Grade', stationIndex['o3Grade'])
        resultDict[stationIndex['stationName']].setdefault('pm10Grade', stationIndex['pm10Grade1h'])
        resultDict[stationIndex['stationName']].setdefault('pm25Grade', stationIndex['pm25Grade1h'])
        resultDict[stationIndex['stationName']].setdefault('no2Grade', stationIndex['no2Grade'])

    return resultDict

def DustNOzoneInfo(lon, lat):
    #미세먼지 탐색을 위한, API 경로
    dustApiAddr = 'http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getCtprvnRltmMesureDnsty'
    global dust_api_keys

    #params 옵션을 이용한 인자 전달 시, 서비스 키가 지속적으로 변경되는 문제가 발생. 따라서 메뉴얼적인 문자 조합으로 조회 수행
    #params = {'serviceKey': dust_api_keys, 'numOfRows':10, 'pageNo':1, 'sidoName':sido, 'ver':1.3}

    sido, stationName = coord2addres( lon, lat)
    targetStation = DetectNearCites(sido, stationName)
    print( sido, stationName )
    #주소를 찾지 못할 경우
    if sido == False:
        return False

    url = dustApiAddr +"?serviceKey="+dust_api_keys+"&sidoName="+sido+"&ver="+str(1.3)

    res = requests.get(url)

    """
    grade값을 4점으로 구성되며, 표는 아래와 같다.
    ______________________________________
    | 등 급 | 좋 음 | 보 통 | 나 쁨 | 매우 나쁨 |
    ______________________________________
    |garade|  1   |  2  |   3  |    4    |
    ______________________________________
    """

    if res.status_code == 200:
        station_info = xml2Dict(res.text)
        Dict_StationInfo = Data2Dict(station_info)

        if targetStation == False:
            for StationInfoIndex in Dict_StationInfo:
                if Dict_StationInfo[StationInfoIndex]['o3Grade'] != None and Dict_StationInfo[StationInfoIndex]['pm10Grade'] != None and \
                    Dict_StationInfo[StationInfoIndex]['pm25Grade'] != None and Dict_StationInfo[StationInfoIndex]['no2Grade'] != None:
                    return Dict_StationInfo[StationInfoIndex]

        else:
            #목록에 같은 동이 있을 경우.
            if stationName in Dict_StationInfo.keys() and Dict_StationInfo[stationName]['o3Grade'] != None and Dict_StationInfo[stationName]['pm10Grade'] != None and Dict_StationInfo[stationName]['pm25Grade1h'] != None and Dict_StationInfo[stationName]['no2Grade'] != None:
                return Dict_StationInfo[stationName]
            else:
                for StationInfoIndex in Dict_StationInfo:
                    if Dict_StationInfo[StationInfoIndex]['o3Grade'] != None and Dict_StationInfo[StationInfoIndex]['pm10Grade'] != None and Dict_StationInfo[StationInfoIndex]['pm25Grade'] != None and Dict_StationInfo[StationInfoIndex]['no2Grade'] != None:
                        return Dict_StationInfo[StationInfoIndex]

    return False

def xml2Dict(xml):
    list2xml = xmltodict.parse(xml)
    list2json = json.dumps(list2xml)
    result = json.loads(list2json)

    return result

if __name__ == "__main__":
    #lon = 128.6865200000000000
    lon = 126.4047260000000000
    #lat = 35.8345900000000000
    lat = 35.8115140000000000

    print(DustNOzoneInfo(lon, lat))